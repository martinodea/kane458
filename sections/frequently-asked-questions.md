## FREQUENTLY ASKED QUESTIONS

**Question:**

What is the countdown time on a KANE458

**Answer:**

There are two levels of countdown (aka fresh air purge) on a KANE458.

From first switch on if ‘cold’ (more than 5&deg;C from the temperature at which calibrated) = 90 secs.

From first switch on if ‘warm’ (within 5&deg;C of the temperature at which calibrated) = 60 secs
