## USING THE FOUR FUNCTION BUTTONS TO TURN ON THE ANALYSER

### Switching ON the Analyser

Rotate the dial to the mode you want to use before switching on. This may eliminate the need for a full countdown in some of the modes and save you time. Press / button to switch the unit ON.  This must be done in fresh air to ensure that the analyser auto calibrates its sensors properly.

When switched on, the analyser beeps and briefly displays software version, date and time.  Its bottom line counts down until the sensors are ready to use.  If the analyser will not auto calibrate, its sensors need to be replaced or recalibrated by an authorised repair centre.  

If an inlet temperature probe (optional) is connected into the T2 socket during its countdown, the measured temperature from the inlet probe will be used as the inlet temperature.

If an inlet temperature probe is not connected to the analyser during countdown the measured temperature from the flue probe will be used as the inlet temperature.

If neither probe is connected during countdown the analyser’s internal ambient temperature will be used as the inlet temperature.  

### Switching OFF the Analyser

Press ![](/images/on-off-button.png) / ![](/images/on-off-1-button.png) & hold for 2 seconds to switch the analyser OFF.

The display counts down from 30 or less with the pump on to clear the sensors with fresh air – If the probe is still connected, make sure analyser and probe are in fresh air.  

Press ![](/images/send-button.png) / ![](/images/print-button.png) if you want to stop the countdown and return to making measurements.

**Note: The analyser will not switch off unless the CO reading is below 40ppm.**

### Torch Light

Press ![](/images/torch-button.png)/ ![](/images/torch-1-button.png) to switch the torch light on and off.

**NOTE:  Use of the torch light significantly increases the current drain on the batteries.**

### Switching PUMP on/off  

The analyser normally operates with the pump on.  

Press ![](/images/pump-button.png) / ![](/images/pump-1-button.png) to switch the pump off and on.

When the pump is switched off `-PO-` is displayed instead of the O<sub>2</sub>, CO & CO<sub>2</sub> readings.  The analyser also displays `PUMP OFF` on the top line approx every 30 seconds.

**NOTE: The pump will automatically switch itself off when the rotary switch is set to Menu, Status, Pressure, Tightness or Differential Temperature.**

### Zeroing the pressure sensor

To re-zero the pressure sensor when “Prs/Temp” is selected on the dial, press and hold ![](/images/pump-button.png) / ![](/images/pump-1-button.png) until the top line display shows CAL ZERO.

Always disconnect the pressure hose before zeroing.

### Printing Data

Press and quickly release ![](/images/send-button.png)/ ![](/images/print-button.png) to start the analyser printing. The analyser displays a series of bars until this is completed. Press and release the key again to abort printing.

Make sure the printer is switched on, ready to accept data and its infrared receiver is in line with the analyser’s emitter (on top of the analyser).  

### Storing a set of readings

Press and hold ![](/images/send-button.png)/ ![](/images/print-button.png) for approx. 2 seconds.

The top line briefly displays the log number.

**Note:  This STORE function is inhibited in normal operation if the pump is switched of**

### Using the Up, Down, and Enter buttons

The function buttons below the symbols ![](/images/up-arrow-button.png) / ![](/images/down-arrow-button.png) / ![](/images/return-button.png) are used to navigate through the menu when the rotary switch is set to MENU – See USING THE MENU, Section 5.  
