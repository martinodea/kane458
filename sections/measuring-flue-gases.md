## MEASURING FLUE GASES

After the countdown is finished and the analyser is correctly set up, put its flue probe into the appliance’s sampling point. The tip of the probe should be at the centre of the flue. Use the flue probe’s depth stop cone to set the position.

With balanced flues, make sure the probe is positioned far enough into the flue so no air can ‘back flush’ into the probe.

NOTE: Ensure that the flue probe handle does not get hot!

![](/images/7-measuring-flue-gases.png)

Make sure you do not exceed the analyser’s operating specifications. In
particular:

- Do not exceed the flue probe’s maximum temperature (600 o C)
- Do not exceed the analyser’s internal temperature operating range
- Do not put the analyser on a hot surface
- Do not exceed the water trap’s levels
- Do not let the analyser’s particle filter become dirty and blocked

View the displayed data to ensure that stable operating conditions have been achieved and the readings are within the expected range.

Press and quickly release ![](/images/send-button.png) / ![](/images/print-button.png) to start the analyser printing. The analyser displays a series of bars until this is completed. Press and release the key again to abort printing.

Make sure the printer is switched on, ready to accept data and its infrared receiver is in line with the analyser’s emitter (on top of the analyser).

**CO PROTECTION PUMP OPERATION**

CO Protection pump operation is totally automatic. When the analyser measures a CO concentration of 2000ppm the pump is switched on. The main pump is switched off and the display shows `P-OFF` until the gas in the sensor is below 2000ppm.

![](/images/7-co-protection-pump-operation.png)

`DILUT` flashes on the screen to indicate that the CO measurement has been diluted.

The time taken for the CO sensor to return to zero takes much longer after it has been exposed to high levels of CO.

The CO high warning still operates whenever the CO reading is above 400 ppm until the dilution pump operates..


