## USING THE ANALYSER:

### COMBUSTION TESTS

Insert the tip of the flue probe into the centre of the flue.  The readings will stabilise within 60 seconds assuming the boiler conditions are stable.
The rotary switch can be used to display the following information:

#### RATIO Display

![](/images/4-1-combustion-tests.png)

Press / to print a full combustion test, (or send to PC via optional Wireless module).

Hold ![](/images/send-button.png) / ![](/images/print-button.png) for 2+ seconds to log a full combustion report.

#### O2/EFF display

![](/images/4-1-o2-display.png)

Press ![](/images/send-button.png) / ![](/images/print-button.png) to print a full combustion test, (or send to PC via optional
Wireless module).

Hold / for 2+ seconds to log a full combustion report.

### AUX Display

![](/images/4-1-aux-display.png)

Press ![](/images/send-button.png) / ![](/images/print-button.png) to print a full combustion test, (or send to PC via optional Wireless module).

Hold  ![](/images/send-button.png) / ![](/images/print-button.png) for 2+ seconds to log a full combustion report.

### View / Printing Overview

The side lights on the display point to the active line.

1. Use ![](/images/up-arrow-button.png) or ![](/images/down-arrow-button.png) to change the pointer. 

2. Press ![](/images/send-button.png) / ![](/images/print-button.png) to select a line. The side lights now flash.

3. Use ![](/images/up-arrow-button.png) or ![](/images/down-arrow-button.png) to scroll or change the selected line.

4. Press ![](/images/send-button.png) / ![](/images/print-button.png) to exit a line.

### To view / print a logged report

Select `MENU > REPORT > COMBUSTION > VIEW`.

The side lights will point to the top line.

1. Press ![](/images/send-button.png) / ![](/images/print-button.png) to select
this line. The side lights will flash.

2. Use ![](/images/up-arrow.png) or ![](/images/down-arrow.png) to scroll or change
the Log No. (If only one report is logged, the number will not change).

3. Press ![](/images/send-button.png) / ![](/images/print-button.png) to confirm a Log No. The side lights will stop flashing.

4. To view logged data press ![](/images/up-arrow.png) or ![](/images/down-arrow.png) to move the pointer to another line.

5. Press ![](/images/send-button.png) / ![](/images/print-button.png). Sidelights will flash on that line.

6. Use ![](/images/up-arrow.png) or ![](/images/down-arrow.png) to scroll through data.

7. To finish, press ![](/images/send-button.png) / ![](/images/print-button.png). Sidelights will flash on that line.

8. Use ![](/images/up-arrow.png) or ![](/images/down-arrow.png) to scroll through data.

9. To finish, press ![](/images/send-button.png) / ![](/images/print-button.png). Sidelights stop flashing.

10. Use ![](/images/up-arrow.png) or ![](/images/down-arrow.png) to scroll down to `PRINT`.

11. Press ![](/images/send-button.png) / ![](/images/print-button.png) to print.

### Viewing/printing a logged combustion test

1. Press ![](/images/send-button.png) / ![](/images/print-button.png) to print the test (or send to PC via optional Wireless module)

### Commissioning Tests

The Commissioning Test is based on TB143.

Rotate the dial to `COM TEST` position and follow the instructions on the screen

#### TEST 1 

_Check the boiler at Max Gas rate._

The boiler is switched on at Max rate.

The analyser is first zeroed in **outside fresh air.**

Once the boiler is stable at max gas flow rate the probe is inserted into the air inlet of the flue and the CO<sub>2</sub> level is measure. The reading needs to be stable and less than or equal to 0.20%.

#### TEST 2

The probe is then inserted into the exhaust outlet of the boiler and the RATIO, CO and CO<sub>2</sub> levels are measured. These levels must be as per manufacturers instruction. Where manufacturers instructions are not available the CO must be less than 350 ppm and the RATIO must be less than 0.0040.

#### TEST 3 

_Checks the boiler at minimum gas flow rate where this is possible._

With the boiler operating stably at minimum gas rate the RATIO, CO and CO<sub>2</sub> levels are measured.

These levels must be as per manufacturers instruction. Where manufacturers’ instructions are not available the CO must be less than 350 ppm and the RATIO must be less than 0.0040.

#### TEST 4 

_Measures Flow and Return Temperatures from the boiler._

All the measured readings are logged and can be printed or transmitted to PC if an optional wireless module is fitted.

### PRESSURE/TEMPERATURE TESTING

![](/images/warning-sign.png) **WARNING**

**NEVER ATTEMPT TO TAKE A PRESSURE READING WITHOUT KNOWING THE MAXIMUM PRESSURE THAT MIGHT BE PRESENT. THIS INSTRUMENT’S PRESSURE TRANSDUCER IS RATED AT 80 MBAR WITH A MAXIMUM OVER RANGE OF 400 MBAR**

Select “Prs/Temp”. The pump stops automatically. Press ![](/images/pump-1-button.png) / ![](/images/pump-button.png) to auto-zero the pressure sensor. Using the black connectors and manometer hose, connect to P1 for single pressure or P1 and P2 for differential pressure.

#### PRS/TEMP display

![](/images/4-1-prs-temp-display.png)

Press ![](/images/send-button.png) / ![](/images/print-button.png) to print a full pressure test, (or send to PC via optional Wireless module).

Press ![](/images/send-button.png) / ![](/images/print-button.png) for 2+ seconds to log a pressure report.

#### Viewing / printing a logged pressure/temp test

Select `MENU > REPORT > PRS-TEMP > VIEW`

Use ![](/images/up-arrow.png) or ![](/images/down-arrow.png) to select the log number to be printed.

Press ![](/images/send-button.png) / ![](/images/print-button.png) to print the test, (or send to PC via optional Wireless module).

![](/images/warning-sign.png) **WARNING**

Before using the KANE458 to measure the pressure of a gas/air ratio valve, read the boiler manufacturer’s instructions thoroughly. If in doubt contact the boiler manufacturer.

After adjusting a gas/air ratio valve it is essential that the CO, CO 2 and CO/CO 2 ratio readings are within the boiler manufacturer’s specified limits.

_**If using larger bore tubing when performing pressure tests:**_

Push ‘orange’ tube over the rim of the spigot to ensure a gas tight seal.
![](/images/4-3-correct-pressure-fitting.png)


This may not produce a gas tight seal.
![](/images/4-3-incorrect-pressure-fitting.png)

### LET-BY & TIGHTNESS TESTING

Select “Tightness”. The pump stops automatically. Press ![](/images/send-button.png) / ![](/images/print-button.png) to auto- zero the pressure sensor. Connect from the test point to P1 using a black connector and manometer hose.

This display shows `LET BY?. Use ![](/images/up-arrow.png) or ![](/images/down-arrow.png) and ![](/images/return-button.png) to start the let-by test. The display shows:

![](/images/4-4-let-by-tightness-test.png)

If the let-by test fails simply move the rotary switch to any position other than “tightness” to abort the test.

If the let-by test passes adjust the gas pressure for the tightness test and press ![](/images/up-arrow-button.png) to start the stabalisation test. The display shows:

![](/images/4-4-stabalisation-test.png)

When complete press ![](/images/return-button.png) to start the tightness test.

![](/images/4-4-start-tightness-test.png)

When complete the display will show:

![](/images/4-4-tightness-test-finish.png)

#### Viewing / printing a logged Let-by and Tightness test

Select `MENU > REPORT > TIGHTNESS > VIEW`

Use ![](/images/up-arrow.png) or ![](/images/down-arrow.png) to select the log number to be printed.

Press ![](/images/send-button.png) / ![](/images/print-button.png) to print the test, (or send to PC via optional Wireless module).

**NOTE: The analyser’s memory can store up to 20 tightness tests. Tightness tests are logged automatically therefore the tightness section of the memory will be full after the 20th tightness test is complete. Before the 21 st tightness test can be performed the tightness section of the memory must be cleared. To do this select `MENU > REPORT > TIGHTNESS > DEL ALL > YES` then press ![](/images/up-arrow.png).**

### ROOM CO TESTING

Select “Room CO” to measure and record CO readings for up to 30 minutes.

![](/images/4-5-room-co-testing.png)

**TEST TYPES**

| &nbsp;   | &nbsp;                                          | &nbsp;                          |
| :----    | :----                                           | ----:                           |
| GENERAL: | 15 minute test with results stored every minute | LIMIT = 10ppm<br>ALARM = 30ppm |
| SWEEP TEST: | 2 minute test with max reading stored at end | LIMIT = 10ppm<br>ALARM = 30ppm |
| MIGRATION TEST: | 15 minute test with results stored every minute | LIMIT = 10ppm<br>ALARM = 30ppm | 
| TYPE C SEALED APPLIANCE: | 15 minute test with results stored every minute | LIMIT = 10ppm<br>ALARM = 30ppm |
| TYPE B BOILER OPEN FLUE: | 15 minute test with results stored every minute | LIMIT = 10ppm<br>ALARM = 30ppm |
| TYPE A COOKER: | 30 minute test with results stored every minute | LIMIT = 30ppm<br>ALARM = 90ppm |
| TYPE A WATER HEATER: | 5 minute test with results stored every minute | LIMIT = 10ppm<br>ALARM = 30ppm |
| TYPE A SPACE HEATER: | 30 minute test with results stored every 1 minute | LIMIT = 10ppm<br>ALARM = 30ppm | 


Press ![](/images/pump-1-button.png) / ![](/images/pump-button.png) to start room CO testing. 

**ROOM CO display**

![](/images/4-5-room-co-display.png)

The user can stop the Room CO test at any time by pressing ![](/images/pump-1-button.png) / ![](/images/pump-button.png).

If not stopped earlier, the Room CO test will automatically end after the designated time.

The CO test series is automatically stored in the memory as a log number.

When completed the log can be printed immediately by pressing ![](/images/return-button.png).


**Viewing / printing a logged Room CO test**

1. Select `MENU > REPORT > ROOM CO > VIEW`

2. When LEDs are not flashing

3. Use the ![](/images/up-arrow-button.png) / ![](/images/down-arrow-button.png) keys to change line.

4. Press ![](/images/return-button.png) to cause the LEDs on that line to flash.  

5. With the LEDs flashing, press ![](/images/up-arrow-button.png) / ![](/images/down-arrow-button.png) to allow the parameter on that line to be changed.    

6. Press ![](/images/return-button.png) to select that change.

7. The LEDs stop flashing

8. Use ![](/images/up-arrow-button.png) / ![](/images/down-arrow-button.png) to change the lines again.

![](/images/4-5-room-co-viewing.png)

With no LEDs flashing

Use the ![](/images/up-arrow-button.png) / ![](/images/down-arrow-button.png) keys to move the lit LEDs to the line you want.

You can change the LOG number and the TEST number so that you can view individual test results.

Press ![](/images/pump-1-button.png) / ![](/images/pump-button.png) to select the line you want and the LEDs will start to flash.

Now use the ![](/images/up-arrow-button.png) / ![](/images/down-arrow-button.png) keys to change the number (the `TEST` number or the `LOG` number)

Press ![](/images/pump-1-button.png) / ![](/images/pump-button.png) when you are happy with the changes. The LEDs will stop flashing. Now use the ![](/images/up-arrow-button.png) / ![](/images/down-arrow-button.png) keys to move the LEDs to the `PRINT` line.

Sending to the printer or wireless device will only occur when you move the LEDs to the print line and press ![](/images/send-button.png) / ![](/images/print-button.png).

Press ![](/images/send-button.png) / ![](/images/print-button.png) to print the test, (or send to PC via optional Wireless module).  

### PRINTOUTS

![](/images/4-6-printouts.png)

![](/images/4-6-printouts-0.png)

![](/images/4-6-printouts-1.png)
