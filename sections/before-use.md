## BEFORE USING THE ANALYSER EVERY TIME:

Check the water trap is empty and the particle filter is not dirty:  

- to empty water trap, unscrew the red screw plug and re-tighten once it is empty.  

- to change the particle filter, remove protective rubber cover, slide the water trap unit from the analyser, remove the particle filter from its spigot and replace.  reconnect the water trap unit and rubber protective cover.

Connect the flue probe hose to the analyser’s flue gas inlet and connect the flue probe’s temperature plug to the t1 socket – check the plug’s orientation is correct -
see the [Analyser Layout and Features](/sections/analyser-layout-features.md) section.  

### FRESH AIR PURGE

Position the flue probe in outside fresh air, then press turn the unit on. The analyser’s pump starts and the analyser auto-calibrates.  When complete:

Select “Ratio” on the dial.  _In fresh air the CO reading should be zero._

Select “O<sub>2</sub>/Eff” on the dial. _In fresh air the O<sub>2</sub> reading should be 20.9% ± 0.3%._

![](/images/2-1-fresh-air-purge.png)

![](/images/warning-sign.png) **WARNING**
This message  indicates that the analyser needs to be reset in fresh air.  To do so, ensure that the analyser is in outside fresh air and press send/print. To perform a manual ‘Gas Zero’, select ‘Ratio’ on the dial, hold down the down key and you will see the message above.

### STATUS DISPLAY

Select “Status” on the dial to view the following:

![](/images/2-2-status-display.png)

![](/images/warning-sign.png) **SAFETY WARNING**

This analyser extracts combustion gases that may be toxic in relatively low concentrations. These gases are exhausted from the back of the instrument. **This analyser  must  only  be  used  in  well-ventilated  locations by trained and competent persons after due consideration of all the potential hazards.**

Users of portable gas detectors are recommended to conduct a “bump” check before relying on the unit to verify an atmosphere is free from hazard.

A “bump” test is a means of verifying that an instrument is working within acceptable limits by briefly exposing to a known gas mixture formulated to change the output of all the sensors present. (This is different from a calibration where the instrument is also exposed to a known gas mixture but is allowed to settle to a steady figure and the reading adjusted to the stated gas concentration of the test gas).
